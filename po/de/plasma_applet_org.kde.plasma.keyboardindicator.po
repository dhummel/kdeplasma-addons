# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Frederik Schwarzer <schwarzer@kde.org>, 2018, 2023.
# Burkhard Lück <lueck@hube-lueck.de>, 2018, 2021.
# Alois Spitzbart <spitz234@hotmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-22 00:38+0000\n"
"PO-Revision-Date: 2023-07-01 18:28+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.07.70\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Tasten"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Anzeigen, wenn aktiviert:"

#: contents/ui/configAppearance.qml:40
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Hochstelltaste"

#: contents/ui/configAppearance.qml:47
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Num-Feststelltaste"

#: contents/ui/main.qml:87
#, kde-format
msgid "Caps Lock activated"
msgstr "Hochstelltaste aktiviert"

#: contents/ui/main.qml:90
#, kde-format
msgid "Num Lock activated"
msgstr "Num-Feststelltaste aktiviert"

#: contents/ui/main.qml:98
#, kde-format
msgid "No lock keys activated"
msgstr "Keine Tastensperre aktiviert"

#~ msgid "Num Lock"
#~ msgstr "Num-Feststelltaste"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: Gesperrt\n"

#~ msgid "Unlocked"
#~ msgstr "Entsperrt"
